const assert = require('assert');

describe('webdriver.io page', () => {
    it('should have the right title', async () => {
        await browser.url('https://uat.atalink.com.vn/m/br');
        const title = await browser.getTitle();
        assert.strictEqual(title, 'ATALINK - Giải pháp quản trị chuỗi cung ứng')
    })
});